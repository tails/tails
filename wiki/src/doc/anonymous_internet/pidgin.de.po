# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2024-05-27 21:53+0000\n"
"PO-Revision-Date: 2024-11-27 11:39+0000\n"
"Last-Translator: jade time <jade4time@proton.me>\n"
"Language-Team: \n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 5.3\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Chatting with Pidgin and OTR\"]]\n"
msgstr "[[!meta title=\"Mit Pidgin und OTR chatten\"]]\n"

#. type: Plain text
#, no-wrap
msgid ""
"For chatting and instant messaging, Tails includes the <span class=\"application\">[Pidgin Instant\n"
"Messenger](https://pidgin.im/)</span>.\n"
msgstr ""
"Zum Chatten und für Sofortnachrichtendienste ist in Tails der <span class=\"application\">[Pidgin\n"
"Sofortnachrichtendienst](https://pidgin.im/)</span> enthalten.\n"

#. type: Plain text
#, no-wrap
msgid "<div class=\"note\">\n"
msgstr "<div class=\"note\">\n"

#. type: Plain text
#, no-wrap
msgid ""
"<p><a href=\"https://dino.im/\"><i>Dino</i></a> is\n"
"being considered as an option to replace <i>Pidgin</i>\n"
"in Tails.</p>\n"
msgstr ""
"<p><a href=\"https://dino.im/\"><i>Dino</i></a> wird \n"
"als Option zum Ersetzen von <i>Pidgin</i> \n"
"in Tails in Betracht gezogen.</p>\n"

#. type: Plain text
#, no-wrap
msgid ""
"<p>You can already try using <i>Dino</i> in Tails. See our\n"
"documentation on [[chatting with <i>Dino</i> and OMEMO|doc/advanced_topics/dino]].</p>\n"
msgstr ""
"<p>Sie können bereits versuchen, <i>Dino</i> in Tails zu verwenden. Sehen Sie sich unsere\n"
"Dokumentation zu [[Chatten mit <i>Dino</i> und OMEMO|doc/advanced_topics/dino]] an.</p>\n"

#. type: Plain text
#, no-wrap
msgid "</div>\n"
msgstr "</div>\n"

#. type: Plain text
msgid ""
"You can use *Pidgin* to connect to [[!wikipedia Internet_Relay_Chat "
"desc=\"IRC\"]] or [[!wikipedia XMPP]] (also known as Jabber) servers. You "
"can have several accounts connected at the same time."
msgstr ""
"Sie können *Pidgin* verwenden, um sich mit [[!wikipedia_de "
"Internet_Relay_Chat desc=\"IRC\"]] oder [[!wikipedia_de XMPP]] (auch bekannt "
"als Jabber) Servern zu verbinden. Sie können mehrere Konten gleichzeitig "
"verbunden haben."

#. type: Plain text
msgid ""
"Even though *Pidgin* supports other protocols, it is only possible to use "
"IRC and XMPP with *Pidgin* in Tails for security reasons."
msgstr ""
"Obwohl *Pidgin* andere Protokolle unterstützt, ist es aus Sicherheitsgründen "
"nur möglich, IRC und XMPP mit *Pidgin* in Tails zu verwenden."

#. type: Plain text
msgid ""
"To start *Pidgin* choose **Applications**&nbsp;▸ **Internet**&nbsp;▸ "
"**Pidgin**."
msgstr ""
"Um *Pidgin* zu starten, wählen Sie **Anwendungen**&nbsp;▸ **Internet**&nbsp;▸"
" **Pidgin**."

#. type: Plain text
msgid ""
"For more documentation, visit the official [*Pidgin* FAQ](https://developer."
"pidgin.im/wiki/Using%20Pidgin)."
msgstr ""
"Für weitere Dokumentation besuchen Sie die offizielle "
"[*Pidgin*-FAQ](https://developer.pidgin.im/wiki/Using%20Pidgin)."

#. type: Plain text
#, no-wrap
msgid "<h1 id=\"otr\">OTR encryption</h1>\n"
msgstr "<h1 id=\"otr\">OTR-Verschlüsselung</h1>\n"

#. type: Plain text
msgid ""
"[OTR (Off-the-Record)](https://otr.cypherpunks.ca/) encryption allows you to "
"have private conversations over instant messaging by providing:"
msgstr ""
"[OTR (Off-the-Record)](https://otr.cypherpunks.ca/) -Verschlüsselung "
"ermöglicht es Ihnen, private Gespräche über Instant Messaging zu führen, "
"indem sie Folgendes bereitstellt:"

#. type: Plain text
msgid "- **Encryption**"
msgstr "- **Verschlüsselung**"

#. type: Plain text
#, no-wrap
msgid "  No one else can read your instant messages.\n"
msgstr "  Niemand anderes kann Ihre Direktnachrichten lesen.\n"

#. type: Plain text
msgid "- **Authentication**"
msgstr "- **Authentifizierung**"

#. type: Plain text
#, no-wrap
msgid "  You are assured the correspondent is who you think it is.\n"
msgstr ""
"  Sie können sicher sein, dass der Korrespondent der ist, von dem Sie "
"denken, dass er es ist.\n"

#. type: Plain text
msgid "- **Deniability**"
msgstr "- **Bestreitbarkeit**"

#. type: Plain text
#, no-wrap
msgid ""
"  The messages you send do not have digital signatures\n"
"  that are checkable by a third party. Anyone can forge messages after\n"
"  a conversation to make them look like they came from you.\n"
"  However, during a conversation, your correspondent is assured\n"
"  the messages are authentic and unmodified.\n"
msgstr ""
"  Die Nachrichten, die Sie senden, haben keine digitalen Signaturen,\n"
"  die von Dritten überprüfbar sind. Jeder kann Nachrichten nach einem\n"
"  Gespräch fälschen, um sie so aussehen zu lassen, als kämen sie von Ihnen.\n"
"  Während eines Gesprächs kann Ihr Gesprächspartner jedoch sicher sein,\n"
"  dass die Nachrichten authentisch und unverändert sind.\n"

#. type: Plain text
msgid "- **Perfect forward secrecy**"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "  If you lose control of your private keys, no previous conversation is compromised.\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<div class=\"caution\">\n"
msgstr "<div class=\"caution\">\n"

#. type: Plain text
#, no-wrap
msgid ""
"<p>OTR is deactivated by default, and your conversations are\n"
"not private.</p>\n"
msgstr ""
"<p>OTR ist standardmäßig deaktiviert und Ihre Unterhaltungen sind \n"
"nicht vertraulich.</p>\n"

#. type: Plain text
#, no-wrap
msgid "<p>File transfers are not encrypted by OTR. OTR only encrypts conversations.</p>\n"
msgstr "<p>Dateiübertragungen werden nicht mit OTR verschlüsselt. OTR verschlüsselt nur Konversationen.</p>\n"

#. type: Plain text
#, no-wrap
msgid "<div class=\"tip\">\n"
msgstr "<div class=\"tip\">\n"

#. type: Plain text
#, no-wrap
msgid ""
"<p>To store your OTR keys and <i>Pidgin</i> settings across different Tails sessions,\n"
"you can turn on the [[Pidgin Internet Messenger|persistent_storage/configure#pidgin]]\n"
"feature of the Persistent Storage.</p>\n"
msgstr ""
"<p>Um Ihre OTR-Schlüssel und <i>Pidgin</i>-Einstellungen über verschiedene "
"Tails-Sitzungen hinweg zu speichern, können Sie die [[Pidgin Internet "
"Messenger|persistent_storage/configure#pidgin]]-Funktion des beständigen "
"Datenspeichers aktivieren.</p>\n"

#. type: Plain text
#, no-wrap
msgid ""
"<p>In a private OTR conversation over IRC, a message sent using the\n"
"<code>/me</code> command is not encrypted. The person receiving\n"
"the message is notified by a warning.</p>\n"
msgstr ""
"<p>In einer privaten Konversation mit OTR über IRC werden Nachrichten, die mit dem\n"
"<code>/me</code> Befehl gesendet werden, nicht verschlüsselt. Die Person,\n"
"die die Nachricht empfängt, wird durch eine Warnung benachrichtigt.</p>\n"

#. type: Title =
#, no-wrap
msgid "IRC servers blocking Tor"
msgstr "Tor blockierende IRC-Server"

#. type: Plain text
msgid ""
"Some IRC servers block connections from Tor because Tor is sometimes used to "
"send spam."
msgstr ""
"Manche IRC-Server blockieren Verbindungen von Tor, da Tor manchmal zum "
"Versenden von Spam benutzt wird."

#. type: Plain text
msgid "- [OFTC and Tor](https://www.oftc.net/Tor/)"
msgstr "- [OFTC und Tor](https://www.oftc.net/Tor/)"

#. type: Plain text
msgid ""
"- [List of IRC/chat networks that block or support Tor](https://gitlab."
"torproject.org/legacy/trac/-/wikis/doc/BlockingIrc)"
msgstr ""
"- [Liste von IRC/Chat-Netzwerken, die Tor blockieren oder "
"unterstützen](https://gitlab.torproject.org/legacy/trac/-/wikis/doc/"
"BlockingIrc)"

#, fuzzy, no-wrap
#~| msgid ""
#~| "To start <span class=\"application\">Pidgin</span> choose\n"
#~| "<span class=\"menuchoice\">\n"
#~| "  <span class=\"guimenu\">Applications</span>&nbsp;▸\n"
#~| "  <span class=\"guisubmenu\">Internet</span>&nbsp;▸\n"
#~| "  <span class=\"guimenuitem\">Pidgin Instant Messenger</span>\n"
#~| "</span> or click on the <span class=\"application\">Pidgin</span> icon in\n"
#~| "the [[<span class=\"guilabel\">Favorites</span> submenu|doc/first_steps/introduction_to_gnome_and_the_tails_desktop#favorites]].\n"
#~ msgid ""
#~ "To start <span class=\"application\">Pidgin</span> choose\n"
#~ "<span class=\"menuchoice\">\n"
#~ "  <span class=\"guimenu\">Applications</span>&nbsp;▸\n"
#~ "  <span class=\"guisubmenu\">Internet</span>&nbsp;▸\n"
#~ "  <span class=\"guimenuitem\">Pidgin Instant Messenger</span>\n"
#~ "</span> or click on the <span class=\"application\">Pidgin</span> icon in\n"
#~ "the [[<span class=\"guilabel\">Favorites</span> submenu|doc/first_steps/desktop#favorites]].\n"
#~ msgstr ""
#~ "Um <span class=\"application\">Pidgin</span> zu starten, wählen Sie\n"
#~ "<span class=\"menuchoice\">\n"
#~ "  <span class=\"guimenu\">Anwendungen</span>&nbsp;▸\n"
#~ "  <span class=\"guisubmenu\">Internet</span>&nbsp;▸\n"
#~ "  <span class=\"guimenuitem\">Pidgin Internet-Sofortnachrichtendienst</span>\n"
#~ "</span> oder wählen Sie das <span class=\"application\">Pidgin</span>-Logo in \n"
#~ "dem [[<span class=\"guilabel\">Favoriten</span>-Untermenü|doc/first_steps/desktop#favorites]].\n"

#, no-wrap
#~ msgid "<span class=\"application\">Off-the-record</span> (<span class=\"application\">OTR</span>) encryption"
#~ msgstr "<span class=\"application\">Off-the-Record</span> (<span class=\"application\">OTR</span>) Verschlüsselung"

#, no-wrap
#~ msgid "Adding support for another protocol"
#~ msgstr "Unterstützung für weitere Protokolle hinzufügen"

#, no-wrap
#~ msgid ""
#~ "For security reasons, it is only possible to use IRC and XMPP with\n"
#~ "<span class=\"application\">Pidgin</span> in Tails. Here are the\n"
#~ "prerequisites to enable another protocol that is supported by <span\n"
#~ "class=\"application\">Pidgin</span> otherwise:\n"
#~ msgstr ""
#~ "Aus Sicherheitsgründen ist es nur möglich, IRC und XMPP mit\n"
#~ "<span class=\"application\">Pidgin</span> in Tails zu benutzen. Hier sind\n"
#~ "die Voraussetzungen, um weitere Protokolle, die von <span\n"
#~ "class=\"application\">Pidgin</span> unterstützt werden, hinzuzufügen:\n"

#~ msgid ""
#~ "If you want to work on this issue, see [[our contribute page|contribute/"
#~ "how/code]]."
#~ msgstr ""
#~ "Wenn Sie an diesem Problem arbeiten wollen, lesen Sie [[unsere Mithilfe-"
#~ "Seite|contribute/how/code]]."

#, no-wrap
#~ msgid "<div class=\"bug\">\n"
#~ msgstr "<div class=\"bug\">\n"

#~ msgid "Predefined accounts\n"
#~ msgstr "Voreingestelle Konten\n"

#~ msgid ""
#~ "One account is configured in <span class=\"application\">Pidgin</span>\n"
#~ "by default:\n"
#~ msgstr ""
#~ "Ein Konto ist standardmäßig in <span class=\"application\">Pidgin</span>\n"
#~ "konfiguriert:\n"

#~ msgid "- `irc.oftc.net` to connect to the OFTC IRC server."
#~ msgstr ""
#~ "- `irc.oftc.net` um eine Verbindung mit dem OFTC IRC-Server herzustellen."

#~ msgid ""
#~ "That account is deactivated when Tails is started. To activate it,\n"
#~ "choose\n"
#~ "<span class=\"menuchoice\">\n"
#~ "  <span class=\"guimenu\">Accounts</span>&nbsp;▸\n"
#~ "  <span class=\"guisubmenu\">Enable Account</span>\n"
#~ "</span>, and select the account that you want to enable in the submenu.\n"
#~ msgstr ""
#~ "Dieses Konto ist beim Start von Tails deaktiviert. Um es zu aktivieren,\n"
#~ "wählen Sie\n"
#~ "<span class=\"menuchoice\">\n"
#~ "  <span class=\"guimenu\">Konten</span>&nbsp;▸\n"
#~ "  <span class=\"guisubmenu\">Konten aktivieren</span>\n"
#~ "</span> und wählen Sie in dem Untermenü das Konto aus, welches Sie "
#~ "aktivieren möchten.\n"

#~ msgid ""
#~ "a. The support in <span class=\"application\">Pidgin</span> for this "
#~ "protocol has been successfully tested\n"
#~ "   in Tails.\n"
#~ "a. Someone volunteers to maintain the corresponding support in Tails\n"
#~ "   on the long term.\n"
#~ "a. Someone has verified that the security record of the desired plugin\n"
#~ "   (including open bugs) is good enough.\n"
#~ msgstr ""
#~ "a. Die Unterstützung des Protokolls in <span "
#~ "class=\"application\">Pidgin</span> wurde erfolgreich in Tails\n"
#~ "   getestet.\n"
#~ "a. Jemand erklärt sich bereit, die entsprechende Betreuung auf lange "
#~ "Sicht\n"
#~ "   in Tails zu bieten.\n"
#~ "a. Jemand hat sichergestellt, dass der Sicherheitsstatus des gewünschten "
#~ "Plugins\n"
#~ "   (inklusive unbehobener Fehler) ausreichend gut ist.\n"

#~ msgid ""
#~ "To learn how to use OTR with <span class=\"application\">Pidgin</span>,\n"
#~ "refer to the documentation from [Security in-a-box: How to Use OTR to\n"
#~ "Initiate a Secure Messaging Session in\n"
#~ "Pidgin](https://securityinabox.org/en/pidgin_securechat).\n"
#~ msgstr ""
#~ "Um zu lernen, wie OTR mit <span class=\"application\">Pidgin</span> "
#~ "benutzt wird,\n"
#~ "verweisen wir auf die Dokumentation von [Security in-a-box: How to Use "
#~ "OTR to\n"
#~ "Initiate a Secure Messaging Session in\n"
#~ "Pidgin (englisch)](https://securityinabox.org/en/pidgin_securechat).\n"
