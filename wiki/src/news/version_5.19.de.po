# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2023-10-31 09:58+0100\n"
"PO-Revision-Date: 2024-11-13 13:37+0000\n"
"Last-Translator: Benjamin Held <Benjamin.Held@protonmail.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 5.3\n"

#. type: Plain text
#, markdown-text, no-wrap
msgid "[[!meta title=\"Tails 5.19\"]]\n"
msgstr "[[!meta title=\"Tails 5.19\"]]\n"

#. type: Plain text
#, markdown-text, no-wrap
msgid "[[!meta date=\"Tue, 31 Oct 2023 12:34:56 +0000\"]]\n"
msgstr "[[!meta date=\"Tue, 31 Oct 2023 12:34:56 +0000\"]]\n"

#. type: Plain text
#, markdown-text, no-wrap
msgid "[[!pagetemplate template=\"news.tmpl\"]]\n"
msgstr "[[!pagetemplate template=\"news.tmpl\"]]\n"

#. type: Plain text
#, markdown-text, no-wrap
msgid "[[!tag announce]]\n"
msgstr "[[!tag announce]]\n"

#. type: Plain text
#, markdown-text, no-wrap
msgid "<h1 id=\"features\">New features</h1>\n"
msgstr "<h1 id=\"features\">Neue Funktionen</h1>\n"

#. type: Plain text
#, markdown-text, no-wrap
msgid "<h2>Closing a Tor circuit from <i>Onion Circuits</i></h2>\n"
msgstr "<h2>Schließen einer Tor-Verbindung aus den <i>Onion Circuits</i></h2>\n"

#. type: Plain text
#, markdown-text
msgid ""
"You can now close a given Tor circuit from the *Onion Circuits* "
"interface. This can help replace a particularly slow Tor circuit or "
"troubleshoot issues on the Tor network."
msgstr ""
"Sie können jetzt einen bestimmten Tor-Circuit über die *Onion Circuits*-"
"Oberfläche schließen. Dies kann dabei helfen, einen besonders langsamen Tor-"
"Circuit zu ersetzen oder Probleme im Tor-Netzwerk zu beheben."

#. type: Plain text
#, markdown-text, no-wrap
msgid "[[!img close_circuit.png link=\"no\" alt=\"\"]]\n"
msgstr "[[!img close_circuit.png link=\"no\" alt=\"\"]]\n"

#. type: Plain text
#, markdown-text
msgid "To close a Tor circuit:"
msgstr "Um einen Tor-Circuit zu schließen:"

#. type: Bullet: '1. '
#, markdown-text
msgid "[[Connect to the Tor network.|doc/anonymous_internet/tor]]"
msgstr "[[Mit dem Tor-Netzwerk verbinden.|doc/anonymous_internet/tor]]"

#. type: Bullet: '1. '
#, markdown-text
msgid ""
"Choose [[!img lib/symbolic/tor-connected.png alt=\"Tor status menu\" "
"link=\"no\" class=\"symbolic\"]]&nbsp;▸ **Open Onion Circuits** in the top "
"navigation bar."
msgstr ""
"Wählen Sie [[!img lib/symbolic/tor-connected.png alt=\"Tor-Statusmenü\" link="
"\"no\" class=\"symbolic\"]]&nbsp;▸ **Onion-Circuits öffnen** in der oberen "
"Navigationsleiste."

#. type: Bullet: '1. '
#, markdown-text
msgid ""
"Right-click (on Mac, click with two fingers) on the circuit that you want to "
"close."
msgstr ""
"Klicken Sie mit der rechten Maustaste (bei Mac mit zwei Fingern klicken) auf "
"den Schaltkreis, den Sie schließen möchten."

#. type: Bullet: '1. '
#, markdown-text
msgid "Choose **Close this circuit** in the shortcut menu."
msgstr "Wählen Sie **Diesen Kanal schließen** im Kontextmenü aus."

#. type: Plain text
#, markdown-text, no-wrap
msgid ""
"   When you close a circuit that is being used by an application, your\n"
"   application gets disconnected from this destination service.\n"
msgstr ""
"   Wenn Sie einen Circuit schließen, der von einer Anwendung verwendet wird,"
"\n"
"   wird Ihre Anwendung von diesem Ziel-Dienst getrennt.\n"

#. type: Plain text
#, markdown-text, no-wrap
msgid ""
"   For example, when you close a circuit while <i>Tor Browser</i> is\n"
"   downloading a file, the download fails.\n"
msgstr ""
"   Zum Beispiel schlägt der Download fehl, wenn Sie einen Zirkel schließen,\n"
"   während der <i>Tor Browser</i> eine Datei herunterlädt.\n"

#. type: Bullet: '1. '
#, markdown-text
msgid ""
"If you connect to the same destination server again, Tor uses a different "
"circuit to replace the circuit that you closed."
msgstr ""
"Wenn Sie sich erneut mit demselben Zielserver verbinden, verwendet Tor einen "
"anderen Circuit, um den Circuit zu ersetzen, den Sie geschlossen haben."

#. type: Plain text
#, markdown-text, no-wrap
msgid ""
"   For example, if you download the same file again, Tor uses a new "
"circuit.\n"
msgstr ""
"   Wenn Sie zum Beispiel die gleiche Datei noch einmal herunterladen, "
"verwendet Tor eine neue Verbindung.\n"

#. type: Plain text
#, markdown-text, no-wrap
msgid "<h2>Addition of <span class=\"code\">sq-keyring-linter</span></h2>\n"
msgstr ""
"<h2>Hinzugefügt von <span class=\"code\">sq-keyring-linter</span></h2>\n"

#. type: Plain text
#, markdown-text
msgid ""
"At the request of people who use [SecureDrop](https://securedrop.org/) to "
"provide secure whistleblowing platforms across the world, we added the "
"[`sq-keyring-linter`](https://tracker.debian.org/pkg/rust-sequoia-keyring-linter)  "
"package. `sq-keyring-linter` improves the cryptographic parameters of PGP "
"keys stored in their airgapped machines."
msgstr ""

#. type: Plain text
#, markdown-text, no-wrap
msgid "<h1 id=\"changes\">Changes and updates</h1>\n"
msgstr "<h1 id=\"changes\">Änderungen und Updates</h1>\n"

#. type: Plain text
#, markdown-text
msgid ""
"- Update *Tor Browser* to "
"[13.0.1](https://blog.torproject.org/new-release-tor-browser-1301)."
msgstr ""
"- Update *Tor Browser* auf [13.0.1](https://blog.torproject.org/new-release-"
"tor-browser-1301)."

#. type: Plain text
#, markdown-text
msgid "- Update the *Tor* client to 0.4.8.7."
msgstr "- Aktualisiert den *Tor*-Client auf 0.4.8.7."

#. type: Plain text
#, markdown-text
msgid ""
"- Update *Thunderbird* to "
"[115.4.1](https://www.thunderbird.net/en-US/thunderbird/115.4.1/releasenotes/)."
msgstr ""
"- Aktualisiert *Thunderbird* auf [115.4.1](https://www.thunderbird.net/en-US/"
"thunderbird/115.4.1/releasenotes/)."

#. type: Plain text
#, markdown-text
msgid "- Update the *Linux* kernel to 6.1.55."
msgstr "- Aktualisiert den *Linux*-Kernel auf 6.1.55."

#. type: Plain text
#, markdown-text, no-wrap
msgid "<h1 id=\"fixes\">Fixed problems</h1>\n"
msgstr "<h1 id=\"fixes\">Behobene Probleme</h1>\n"

#. type: Plain text
#, markdown-text
msgid ""
"For more details, read our [[!tails_gitweb debian/changelog "
"desc=\"changelog\"]]."
msgstr ""
"Für weitere Details lesen Sie unser [[!tails_gitweb debian/changelog desc="
"\"Änderungsprotokoll\"]]."

#. type: Plain text
#, markdown-text, no-wrap
msgid "<h1 id=\"issues\">Known issues</h1>\n"
msgstr "<h1 id=\"issues\">Bekannte Probleme</h1>\n"

#. type: Plain text
#, markdown-text
msgid "None specific to this release."
msgstr "Keine spezifischen Angaben zu dieser Version."

#. type: Plain text
#, markdown-text
msgid "See the list of [[long-standing issues|support/known_issues]]."
msgstr "Siehe die Liste der [[Altlasten|support/known_issues]]."

#. type: Plain text
#, markdown-text, no-wrap
msgid "<h1 id=\"get\">Get Tails 5.19</h1>\n"
msgstr ""

#. type: Title ##
#, markdown-text, no-wrap
msgid "To upgrade your Tails USB stick and keep your Persistent Storage"
msgstr ""
"Um Ihren Tails-USB-Stick zu aktualisieren und Ihren beständigen "
"Datenspeicher beizubehalten"

#. type: Plain text
#, markdown-text
msgid "- Automatic upgrades are available from Tails 5.0 or later to 5.19."
msgstr ""

#. type: Plain text
#, markdown-text, no-wrap
msgid ""
"  You can [[reduce the size of the download|doc/upgrade#reduce]] of future\n"
"  automatic upgrades by doing a manual upgrade to the latest version.\n"
msgstr ""
"  Sie können [[die Größe des Downloads|doc/upgrade#reduce]] von zukünftigen\n"
"  automatischen Upgrades reduzieren, indem Sie ein manuelles Upgrade auf die "
"neueste Version durchführen.\n"

#. type: Bullet: '- '
#, markdown-text
msgid ""
"If you cannot do an automatic upgrade or if Tails fails to start after an "
"automatic upgrade, please try to do a [[manual "
"upgrade|doc/upgrade/#manual]]."
msgstr ""
"Wenn Sie ein automatisches Upgrade nicht durchführen können oder Tails nach "
"einem automatischen Upgrade nicht startet, versuchen Sie bitte ein [["
"manuelles Upgrade|doc/upgrade/#manual]]."

#. type: Title ##
#, markdown-text, no-wrap
msgid "To install Tails on a new USB stick"
msgstr "Um Tails auf einem neuen USB-Stick zu installieren"

#. type: Plain text
#, markdown-text
msgid "Follow our installation instructions:"
msgstr "Befolgen Sie unsere Installationsanweisungen:"

#. type: Bullet: '  - '
#, markdown-text
msgid "[[Install from Windows|install/windows]]"
msgstr "[[Installieren von Windows|install/windows]]"

#. type: Bullet: '  - '
#, markdown-text
msgid "[[Install from macOS|install/mac]]"
msgstr "[[Installieren von macOS|install/mac]]"

#. type: Bullet: '  - '
#, markdown-text
msgid "[[Install from Linux|install/linux]]"
msgstr "[[Installieren von Linux|install/linux]]"

#. type: Bullet: '  - '
#, markdown-text
msgid ""
"[[Install from Debian or Ubuntu using the command line and "
"GnuPG|install/expert]]"
msgstr ""
"[[Installation von Debian oder Ubuntu über die Kommandozeile und GnuPG|"
"install/expert]]"

#. type: Plain text
#, markdown-text, no-wrap
msgid ""
"<div class=\"caution\"><p>The Persistent Storage on the USB stick will be "
"lost if\n"
"you install instead of upgrading.</p></div>\n"
msgstr ""
"<div class=\"caution\"><p>Der beständige Datenspeicher auf dem USB-Stick "
"geht verloren,\n"
"wenn Sie installieren anstatt zu aktualisieren.</p></div>\n"

#. type: Title ##
#, markdown-text, no-wrap
msgid "To download only"
msgstr "Nur herunterladen"

#. type: Plain text
#, markdown-text
msgid ""
"If you don't need installation or upgrade instructions, you can download "
"Tails 5.19 directly:"
msgstr ""
"Wenn Sie keine Installations- oder Upgrade-Anleitungen benötigen, können Sie "
"Tails 5.19 direkt herunterladen:"

#. type: Bullet: '  - '
#, markdown-text
msgid "[[For USB sticks (USB image)|install/download]]"
msgstr "[[Für USB-Sticks (USB-Image)|install/download]]"

#. type: Bullet: '  - '
#, markdown-text
msgid "[[For DVDs and virtual machines (ISO image)|install/download-iso]]"
msgstr "[[Für DVDs und virtuelle Maschinen (ISO-Image)|install/download-iso]]"
