# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2018-05-08 15:49+0300\n"
"PO-Revision-Date: 2018-05-21 08:29+0000\n"
"Last-Translator: Joaquín Serna <bubuanabelas@cryptolab.net>\n"
"Language-Team: Spanish <http://translate.tails.boum.org/projects/tails/"
"debian_requirementsinline/es/>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 2.10.1\n"

#. type: Plain text
msgid "These instructions require:"
msgstr "Estas instrucciones requieren:"

#. type: Bullet: '  - '
msgid "Debian 9 (Stretch) or later"
msgstr "Debian 9 (Stretch) o posterior"

#. type: Bullet: '  - '
msgid "Ubuntu 16.04 (Xenial Xerus) or later"
msgstr "Ubuntu 16.04 (Xenial Xerus) o posterior"

#. type: Bullet: '  - '
msgid "Linux Mint 18 (Sarah) or later"
msgstr "Linux Mint 18 (Sarah) o posterior"

#. type: Plain text
msgid ""
"If your version of Debian, Ubuntu, or Linux Mint is not supported, you can "
"follow the instructions for [[other Linux distributions|install/linux/usb-"
"overview]] instead. But they are more complicated and require two USB sticks "
"instead of one."
msgstr ""
"Si tu versión de Debian, Ubuntu o Linux Mint no es soportada, puedes seguir "
"las instrucciones para [[otras distribuciones de Linux|install/linux/usb-"
"overview]]. Pero son más complicadas y requieren dos memorias USB en vez de "
"una."

#. type: Plain text
#, no-wrap
msgid "<div class=\"tip debian\">\n"
msgstr "<div class=\"tip debian\">\n"

#. type: Plain text
#, no-wrap
msgid ""
"<p>To know your version of Debian or Ubuntu, open\n"
"<span class=\"application\">System Settings</span> and click on\n"
"<span class=\"guilabel\">Details</span>. For Debian, refer to the\n"
"<span class=\"guilabel\">Base system</span> information.</p>\n"
msgstr ""
"<p>Para saber tu versión de Debian o Ubuntu, abre\n"
"<span class=\"application\">Configuración del Sistema</span> y haz click en\n"
"<span class=\"guilabel\">Detalles</span>. Para Debian, ve a la\n"
"información de <span class=\"guilabel\">Sistema base</span>.</p>\n"

#. type: Plain text
#, no-wrap
msgid ""
"<p>To know your version of Linux Mint, open the application\n"
"<span class=\"application\">Welcome Screen</span>.\n"
msgstr ""
"<p>Para saber tu versión de Linux Mint, abre la aplicación\n"
"<span class=\"application\">Pantalla de Bienvenida</span>.\n"

#. type: Plain text
#, no-wrap
msgid "</div>\n"
msgstr "</div>\n"

#. type: Plain text
#, no-wrap
msgid "<div class=\"tip expert\">\n"
msgstr "<div class=\"tip expert\">\n"

#. type: Plain text
#, no-wrap
msgid "<p>To know your version of Debian, Ubuntu, or Linux Mint, execute:</p>\n"
msgstr "<p>Para conocer tu versión de Debian, Ubuntu, o Linux Mint, ejecuta:</p>\n"

#. type: Plain text
#, no-wrap
msgid "<pre><code>lsb_release -a</code></pre>\n"
msgstr "<pre><code>lsb_release -a</code></pre>\n"
